import React, {Component} from "react";
import {connect} from "react-redux";
import {handleDelete, mapStateToProps} from '../store/actions';

class TextComponent extends Component {

    render() {
        const {text, items, color, handleDelete} = this.props;
        const itemsResutl = [];

        for (let i = 0; i < items.length; i++) {
            itemsResutl.push(
                <div
                    key={i}
                    onClick={() => {
                        handleDelete(i);
                    }}>
                    {(i + 1) + ". "}{items[i]}
                </div>)
        }
        return (
            <div style={{color: color}}>
                {itemsResutl}
                {text !== "" ? (items.length + 1) + ". " + text : ""}
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        handleDelete: (i) => {
            dispatch(handleDelete(i))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(TextComponent);