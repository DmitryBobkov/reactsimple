import React, {Component} from 'react';
import ButtonComponent from './ButtonComponent';
import TextComponent from "./TextComponent";
import WriteLineComponent from "./WriteLineComponent";

class MainComponent extends Component {
    render() {
        return (
            <div>
                <TextComponent/>
                <WriteLineComponent/>
                <ButtonComponent
                    nameButton="button_Red"
                    colorButton="Red"/>
                <ButtonComponent
                    nameButton="button_Green"
                    colorButton="Green"/>
                <ButtonComponent
                    nameButton="button_Blue"
                    colorButton="Blue"
                />
            </div>
        )
    }
}

export default MainComponent;