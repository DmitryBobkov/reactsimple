import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {handleChange, handleSubmit, mapStateToProps} from "../store/actions";

class WriteLineComponent extends Component {
    render() {
        const {text, handleChange, handleSubmit} = this.props;

        return (
            <Fragment>
                <input
                    onChange={(event) => {
                        handleChange(event);
                    }}
                    value={text}/>
                <button
                    onClick={() => {
                        handleSubmit();
                    }}>
                    New line
                </button>
            </Fragment>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        handleChange: (event) => {
            dispatch(handleChange(event))
        },
        handleSubmit: () => {
            dispatch(handleSubmit())
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(WriteLineComponent);