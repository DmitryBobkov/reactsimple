import React, {Component} from "react";
import {connect} from "react-redux";
import {changeColor, mapStateToProps} from '../store/actions';

class ButtonComponent extends Component {
    render() {
        const {nameButton, colorButton, changeColor} = this.props;

        return (
            <div>
                <button
                    onClick={() => {
                        changeColor(colorButton);
                    }}
                    style={{color: colorButton}}>
                    {nameButton}
                </button>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        changeColor: (color) => {
            dispatch(changeColor(color))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ButtonComponent);