export const CHANGE_COLOR = 'CHANGE_COLOR';
export const HANDLE_DELETE = 'HANDLE_DELETE';
export const HANDLE_CHANGE = 'HANDLE_CHANGE';
export const HANDLE_SUBMIT = 'HANDLE_SUBMIT';

export const mapStateToProps = (state) => {
    return {
        color: state.color,
        items: state.items,
        text: state.text
    }
};

export const changeColor = (color) => {
    return {
        type: CHANGE_COLOR,
        color: color
    }
};

export const handleDelete = (itemIndex) => {
    return {
        type: HANDLE_DELETE,
        itemIndex: itemIndex
    }
};

export const handleChange = (event) => {
    return {
        type: HANDLE_CHANGE,
        text: event
    }
};

export const handleSubmit = () => {
    return {
        type: HANDLE_SUBMIT
    }
};