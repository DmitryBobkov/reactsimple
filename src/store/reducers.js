import {CHANGE_COLOR, HANDLE_DELETE, HANDLE_CHANGE, HANDLE_SUBMIT} from '../store/actions';

const initialState = {
    color: null,
    items: [],
    text: ''
};

export const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case CHANGE_COLOR:
            return {
                ...state,
                color: action.color
            };
        case HANDLE_DELETE:
            const itemsResult = [...state.items];
            if (itemsResult.length !== action.itemIndex && itemsResult.length - 1 !== action.itemIndex) {
                itemsResult.splice(action.itemIndex, 1);
            }
            return {
                ...state,
                items: itemsResult
            };
        case HANDLE_CHANGE:
            return {
                ...state,
                text: action.text.target.value
            };
        case HANDLE_SUBMIT:
            return {
                ...state,
                items: state.text !== '' ? state.items.concat(state.text) : state.items, text: ''
            };
        default:
            return state;
    }
};